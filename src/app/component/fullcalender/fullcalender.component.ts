import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { CalenderService } from './service';
import { StudentModel, ClassesModel } from './student.model';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import { NgxSpinnerService } from 'ngx-spinner';
import { formatDate, DatePipe } from '@angular/common';
export class ICancelProp {
  student_id: number;
  class_id: number;
  subject_id: number;
}
declare var $;
@Component({
  selector: 'app-fullcalender',
  templateUrl: './fullcalender.component.html',
  styleUrls: ['./fullcalender.component.scss'],
})
export class FullcalenderComponent implements OnInit, AfterViewInit {
  studentList: Array<StudentModel> = [];
  classes: Array<any> = [];
  calendarOptions: CalendarOptions = { initialView: 'timeGridWeek', };
  pipe: any;
  childrenIds: any;
  selectedChildInfo: ICancelProp = new ICancelProp();
  isReSchedule = false;
  timeSlotStartDate: any;
  timeSlotEndDate: any;
  currentDate = new Date();
  timeSlots = [];
  constructor(
    private calenderService: CalenderService,
    private spinner: NgxSpinnerService

  ) {
    this.pipe = new DatePipe('en-US');
  }

  ngOnInit() {
    this.getChildrenList();
   

  }

  ngAfterViewInit() {


  }

  continueRescdule = () => {
    $('#reschedule').modal('show');
  }

  getChildrenList = () => {
    this.calenderService.getChildrenList(127).subscribe(res => {
      if (res.students) {
        this.studentList = res.students;
        this.childrenIds = this.studentList[0].id;
        this.onClickWeek();
        // this.getChildClassDetail(this.studentList[0].id);
      }
    });
  }



  handleDateClick(arg) {
    alert('date click! ' + arg.dateStr);
  }

  getChildClassDetail = (startDate, endDate) => {
    this.spinner.show();

    this.calenderService.getChildClassDetail(this.childrenIds, startDate, endDate).subscribe(res => {
      if (res.classes) {
        this.classes = res.classes[this.childrenIds];
        this.classes.forEach(cls => {
          if (cls) {
            cls.start = cls.class_date.split('T')[0] + 'T' + cls.start_time;
            cls.end = cls.class_date.split('T')[0] + 'T' + cls.end_time;
            cls.title = cls.gc_event_title;
            // ? cls.gc_event_title + '(' + cls.class_id + ')' : '-' + cls.class_id;
          }
        });
        this.calendarInit();
      }
    });

  }


  calendarInit = () => {
    let ref = this;
    this.calendarOptions = {
      themeSystem: 'bootstrap4',

      customButtons: {
        myCustomButton: {
          text: 'Reading Genie',
          click: function a() {
            alert('clicked the custom button!');
          }
        },
        subject1: {
          text: 'Common Core',
          click: function b() {
            alert('clicked the code button!');
          }
        },
        subject2: {
          text: 'Code',
          click: function c() {
            alert('clicked the common core button!');
          }
        },
        subject3: {
          text: 'Abacus Math',
          click: function c() {
            alert('clicked the common core button!');
          }
        }
      },
      headerToolbar: {
        // subject3,subject2,subject1,myCustomButton
        right: 'prev,next',
        center: '',
        left: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      events: this.classes,
      eventClick: function a(eventD) {
        ref.selectedChildInfo.student_id = +eventD.event._def.extendedProps.student_id;
        ref.selectedChildInfo.class_id = +eventD.event._def.publicId;
        ref.selectedChildInfo.subject_id = eventD.event._def.extendedProps.subject_id;
      }


    };

    this.eventInit();
    this.spinner.hide();
  }

  eventInit = () => {
    setTimeout(() => {
      const month = document.getElementsByClassName('fc-dayGridMonth-button');
      const week = document.getElementsByClassName('fc-timeGridWeek-button');
      const day = document.getElementsByClassName('fc-timeGridDay-button');
      const previous = document.getElementsByClassName('fc-prev-button');
      const next = document.getElementsByClassName('fc-next-button');

      for (let i = 0; i < month.length; i++) {
        month[i].addEventListener("click", (event: Event) => {
          this.onClickMonth();
        });
      }
      for (let i = 0; i < week.length; i++) {
        week[i].addEventListener("click", (event: Event) => {
          this.onClickWeek();
        });
      }

      for (let i = 0; i < day.length; i++) {
        day[i].addEventListener("click", (event: Event) => {
          this.onClickDay();
        });
      }

      for (let i = 0; i < previous.length; i++) {
        previous[i].addEventListener("click", (event: Event) => {
          const to = this.currentDate.setTime(this.currentDate.getTime() - (this.currentDate.getDay() ? this.currentDate.getDay() : 7) * 24 * 60 * 60 * 1000);
          const from = this.currentDate.setTime(this.currentDate.getTime() - 6 * 24 * 60 * 60 * 1000);
          this.formatDate(from, to);
          this.currentDate = new Date(from);
        });
      }

      for (let i = 0; i < next.length; i++) {
        next[i].addEventListener("click", (event: Event) => {
          console.log(event);
        });
      }

    }, 500);


    setTimeout(() => {
      if (document.getElementsByClassName('dotIcon').length === 0) {
        document.querySelectorAll('.fc-event-title-container')[0].innerHTML += '<div class="dotIcon" style=" color: #8BC402;font-size: 18px;cursor:pointer" ><i class="fa fa-ellipsis-v"></i></div>';
        const dotIcon = document.getElementsByClassName('dotIcon');

        for (let i = 0; i < dotIcon.length; i++) {
          dotIcon[i].addEventListener("click", (event: Event) => {
            $('#exampleModalCenter').modal('show');
          });
        }
      }

    }, 500);
  }


  onClickWeek = () => {
    // alert('w');
    const curr = new Date(); // get current date
    const first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    const last = first + 6; // last day is the first day + 6

    const startDate = new Date(curr.setDate(first)).toUTCString();
    const endDate = new Date(curr.setDate(last)).toUTCString();
    this.formatDate(startDate, endDate);

  }

  onClickMonth = () => {
    const date = new Date();
    const startDate = new Date(date.getFullYear(), date.getMonth(), 1);
    const endDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    this.formatDate(startDate, endDate);
  }

  onClickDay = () => {
    const startDate = new Date();
    const endDate = new Date();
    this.formatDate(startDate, endDate);
  }

  formatDate(startDate, endDate) {
    startDate = this.pipe.transform(startDate, 'yyyy-MM-dd');
    endDate = this.pipe.transform(endDate, 'yyyy-MM-dd');
    this.getChildClassDetail(startDate, endDate);
  }

  cancelClass = () => {
    this.calenderService.cancelClass(this.selectedChildInfo).subscribe(res => {
      if (res) {
        this.onClickWeek();
        $('#exampleModalCenter').modal('hide');
      }
    });
  }

  reSchedule = () => {
    this.isReSchedule = true;
    $('#exampleModalCenter').modal('hide');
    this.onDateChange(new Date());
  }

  onDateChange = (date) => {
    // const curr = date;
    const first = date.getDate() - date.getDay();
    const last = first + 6; // last day is the first day + 6
    const endDate = new Date(new Date(date).setDate(last)).toUTCString();
    const startDate = this.pipe.transform(date, 'MM/dd/yyyy');
    const weekEnd = this.pipe.transform(endDate, 'MM/dd/yyyy');
    this.calenderService.getAvailableSlot(this.selectedChildInfo.subject_id, startDate, weekEnd).subscribe(res => {
      if (res.results.available_classes.length > 0) {
        const groups = res.results.available_classes.reduce((slot, game) => {
          const dates = game.class_date.split('T')[0];
          if (!slot[dates]) {
            slot[dates] = [];
          }
          slot[dates].push(game);
          return slot;
        }, {});

        // Edit: to add it in the array format instead
        const groupArrays = Object.keys(groups).map((newDate) => {
          const modifiedDate = this.pipe.transform(newDate, 'MMMM d, EEEE');
          return {
            modifiedDate,
            slots: groups[newDate]
          };
        });

        this.timeSlots = groupArrays;
      }
    });
  }

}
