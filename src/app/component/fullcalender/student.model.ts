export class StudentModel {
    first_name: string;
    id: number;
    last_name: string;
}

export class ClassesModel {
    activity: string;
    class_date: string;
    class_id: number;
    class_status: string;
    comments: string;
    create_date: string;
    created_by: number;
    duration: number;
    end_time: string;
    finalized_by: string;
    finalized_timestamp: string;
    gc_event_id: string;
    gc_event_title: string;
    id: number;
    is_finalized: boolean;
    location: number;
    max_capacity: number;
    modified_by: number;
    modified_date: string;
    replacement_rollout: string;
    room: number;
    show_while_cancelled: boolean;
    staff: number;
    start_time: string;
    subject: number;
    weekly_report_changed_by: string;
    weekly_report_changed_date: string;
    weekly_report_created_by: string;
    weekly_report_date: string;
    weekly_review_timestamp: string;
    weekly_reviewed_by_staff: string;
    writing_prompt: string;
    yoga: boolean;
    start: string;
    end: string;
    title: string;
}