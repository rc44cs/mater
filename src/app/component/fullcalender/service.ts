import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';



@Injectable({
    providedIn: 'root'
})
export class CalenderService {

    constructor(private http: HttpClient) {
    }

    getChildrenList(parentID) {
        const url = 'https://beacondev.mathgenie.com/api/parents/student-list-for-parent/' + parentID;
        return this.http.get<any>(url)
            .pipe(map((body) => body));
    }

    getChildClassDetail(ids, start, end) {
        const url = 'https://beacondev.mathgenie.com/api/parents/student-class-list/?student_id=' + ids + '&start_date=' + start + '&end_date=' + end;
        return this.http.get<any>(url)
            .pipe(map((body) => body));
    }

    cancelClass(payload) {
        const url = 'https://beacondev.mathgenie.com/api/parents/cancel-child-class/';
        return this.http.post<any>(url, payload)
            .pipe(map((body) => body));
    }

    getAvailableSlot(subjectId, start, end) {
        const url = 'https://beacondev.mathgenie.com/api/parents/available-classes/?subject_id=' + subjectId + '&start_date=' + start + '&end_date=' + end;
        return this.http.get<any>(url)
            .pipe(map((body) => body));
    }

}
