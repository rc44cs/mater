import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Injectable()
export class ApiHttpInterceptor implements HttpInterceptor {

  constructor(
    private toastr: ToastrService,
    public router: Router,
  ) { }
  errorMessage = '';
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // const apiReq = request.clone({ url: environment.apiUrl + `${request.url}` });
    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      }
    }, (err: any) => {
      this.errorMessage = '';
      if (err instanceof HttpErrorResponse) {
        // this.count1++;
        // console.log('end' + this.count1)
        if (err.status === 401) {
          localStorage.removeItem('Authorization');
          localStorage.removeItem('tabs');
          this.router.navigate(['/sign-in']);

        } else if (err.status === 403) {
            this.showErrorMessage('Internal server error');

        } else if (err.status === 0) {                /* Api Connection Refused*/
          //  this.showErrorMessage('Server down!');
        } else if (err.status === 404) {
          this.showErrorMessage(err.statusText);
          // return Observable.throw(err);
        } else if (err.status === 400) {
          /*Bad Request*/   const errorRes = err.error.non_field_errors.filter((elem) => {
          this.errorMessage = this.errorMessage + elem;
        });
          this.showErrorMessage(this.errorMessage);
        } else if (err.status === 500) {           /* Internal Server error*/
          // this.showErrorMessage(err.statusText);
        }
      }
    }
    );
  }

  private showErrorMessage(errorMessage) {
    this.toastr.error(errorMessage);
  }
}
