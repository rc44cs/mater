import { Component, OnInit } from '@angular/core';
import { AuthServices } from '../auth.service';
import { LoginModel } from './login.model';
import { Router, ActivatedRoute } from '@angular/router';
// import { Storage } from '@ionic/storage';
import { SocialAuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [SocialAuthService]
})
export class LoginComponent implements OnInit {
  logo = '../../assets/images/logo.svg';
  loginInfo: LoginModel = new LoginModel();
  constructor(
    private authServices: AuthServices,
    private route: ActivatedRoute,
    private router: Router,
    // private storage: Storage,
    private socialauthService: SocialAuthService
  ) { }

  ngOnInit() { }

  login = () => {
    this.authServices.login(this.loginInfo).subscribe(res => {
      if (res) {
        // this.storage.set('userInfo', res);
        this.router.navigate(['./calender']);
      }
    });
  }

  signInWithGoogle(): void {
    this.socialauthService.signIn(GoogleLoginProvider.PROVIDER_ID).then(res => {
      if (res) {
        let data = {
          'access_token': res.authToken
        }
        this.authServices.googleLogin(data).subscribe(res => {
          console.log(res);
        })

      }
    });
  }

}
