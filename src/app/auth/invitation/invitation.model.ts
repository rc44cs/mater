export class VerifyUserModel {
    token: string;
    password: string;
    email: string;
    status: string;
    emailStep: boolean = false;
    passwordStep: boolean = false;
}
