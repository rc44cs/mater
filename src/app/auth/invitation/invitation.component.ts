import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthServices } from '../auth.service';
import { VerifyUserModel } from './invitation.model';
import { SocialAuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss'],
  providers: [SocialAuthService]
})
export class InvitationComponent implements OnInit {
  logo = '../../assets/images/logo.svg';
  token: string = null;
  verifyUser: VerifyUserModel = new VerifyUserModel();
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthServices,
    private socialauthService: SocialAuthService
  ) {

    this.route.queryParams.subscribe(params => {
      this.verifyUser.token = params['token'];
      alert(this.verifyUser.token);
    });
  }

  ngOnInit() {
    //this.verifyUser.token = this.route.snapshot.paramMap.get('token');
    this.verifyUserToken();
  }

  verifyUserToken = () => {
    //  this.verifyUser.token = 'UfFiQkVG5qTRipabCS+wcns/YaF91ksc5boZ8TTITQQ/nVVD10Yk/Umx/6iqLBXPL3m8QNSwnJwZeZlF6YuThonsI9zT3JFdnKdpFnZuKAk=';
    this.authService.verifyUser(this.verifyUser).subscribe(res => {
      if (res.status === 'Verfied') {
        this.verifyUser.email = res.parent_email;
        this.verifyUser.token = res.token;
        this.verifyUser.status = res.status;
      }
    });
  }



  registerWithGoogle(): void {
    this.socialauthService.signIn(GoogleLoginProvider.PROVIDER_ID).then(res => {
      if (res) {
        let data = {
          'access_token': res.authToken,
          'token': this.verifyUser.token
        }
        this.authService.googleLogin(data).subscribe(res => {
          console.log(res);
        })
      }
    });
  }

  customRegister = () => {
    if (this.verifyUser.status === 'Verfied') {
      this.verifyUser.emailStep = true;
    }
  }

  next = () => {
    this.verifyUser.emailStep = false;
    this.verifyUser.passwordStep = true;
  }

  submit = () => {
    this.authService.registerParent(this.verifyUser).subscribe(res => {
      if (res) {
        this.router.navigate(['./auth/login']);
      }
    })
  }

}
