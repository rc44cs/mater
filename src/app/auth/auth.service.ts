import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';



@Injectable({
    providedIn: 'root'
})
export class AuthServices {

    constructor(private http: HttpClient) {
    }

    verifyUser(payload) {
        const url = 'https://beacondev.mathgenie.com/api/parents/verify-email/';
        return this.http.post<any>(url, payload)
            .pipe(map((body) => body));
    }

    registerParent(payload) {
        const url = 'https://beacondev.mathgenie.com/api/parents/registration/';
        return this.http.post<any>(url, payload)
            .pipe(map((body) => body));
    }

    login(payload) {
        const url = 'https://beacondev.mathgenie.com/api/parents/custom-login/';
        return this.http.post<any>(url, payload)
            .pipe(map((body) => body));
    }


    googleLogin(authToken) {
        const url = 'https://beacondev.mathgenie.com/api/parents/register/social/google/login/'

        return this.http.post<any>(url,authToken)
            .pipe(map((body) => body));
    }
    googleSignUp(authToken) {
        const url = 'https://beacondev.mathgenie.com/api/parents/register/social/google/login/'

        return this.http.post<any>(url,authToken)
            .pipe(map((body) => body));
    }

}
``