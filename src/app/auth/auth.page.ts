import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  logo = '../../assets/images/logo.svg';
  constructor(
    private toastr: ToastrService
  ) { }

  ngOnInit() {
   
  }

}
